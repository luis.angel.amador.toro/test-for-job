let height = document.getElementById("height").value = 1170
let svg = document.getElementById("svg")

function changeHeight(e){
    height = parseInt(e);
    if(height < 1){
        alert("The value of the viewbox cannot be less than 1");
        document.getElementById("height").value = 1;
    };
    svg.setAttribute("viewBox", `0 0 1370 ${height}`);
};